<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class Routelocation extends Model
{
     protected $fillable=['from','to','arrival','depature'];

     protected $table = 'routeloations';
}
