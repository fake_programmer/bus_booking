<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class Route extends Model
{
    protected $fillable=['route_location','arrival_time','depature_time','stop_no'];
}
