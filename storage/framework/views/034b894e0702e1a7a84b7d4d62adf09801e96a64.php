<?php $__env->startSection('content'); ?>


<style>
	.bus-container {
		width: 200px;
		margin: 40px auto 40px;
		background: #c5c5c5;;
		padding: 20px;
	}
	.bus-container > .row {
		margin-bottom: 10px;
	}
	.seat {
		text-align: center;
		height: 40px;
		width: 40px;
		line-height: 40px;
		background: #333;
		border-radius: 3px;
	}
	.seat a {
		color: #fff;
	}
	.double .row {
		margin: 0;
	}
	.seat input[type="checkbox"]{
		position: absolute;
		opacity: 0;
	}
	.seat label {
		cursor: pointer;
		color:#fff;
	}
	.seat input[type="checkbox"]:checked + label {
		background: #fff;
		color: #333;
		font-weight: bold;
		height: 40px;
		width: 40px;
		border-radius: 3px;
	}
	.seat label:before {
		display: inline-block;
		text-rendering: auto;

	}
</style>

<form action="<?php echo e(route('showinfo')); ?>" method="post" role="form">
	<?php echo csrf_field(); ?>
	<input
			type="hidden" value="<?php echo e($id); ?>" name="bus_id">
	
<div class="container">
	<div class="bus-container">
		<div class="row">
			<div class="col-md-4 single">
				<div class="seat" <?php if(in_array('A1',$reserved_seat)): ?> style="background-color: red"  <?php endif; ?>>
					<input <?php if(in_array('A1',$reserved_seat)): ?> disabled  <?php endif; ?> type="checkbox" id="A1" value="A1" name="seat[]">
					<label for="A1">A1</label>
				</div>
			</div>
			<div class="col-md-8 double">
				<div class="row">
					<div class="col-md-6">
				<div class="seat" <?php if(in_array('A2',$reserved_seat)): ?> style="background-color: red"  <?php endif; ?>>
					<input <?php if(in_array('A2',$reserved_seat)): ?> disabled  <?php endif; ?> type="checkbox" id="A2" value="A2"  name="seat[]">
					<label for="A2">A2</label>
						</div>
					</div>
					<div class="col-md-6">
				<div class="seat" <?php if(in_array('A3',$reserved_seat)): ?> style="background-color: red"  <?php endif; ?>>
					<input <?php if(in_array('A3',$reserved_seat)): ?> disabled  <?php endif; ?> type="checkbox" id="A3" value="A3"  name="seat[]">
					<label for="A3">A3</label>
						</div>

					</div>
				</div>
			</div>
		</div>


		<!-- B Row -->
		<div class="row">
			<div class="col-md-4 single">
				<div class="seat" <?php if(in_array('B1',$reserved_seat)): ?> style="background-color: red"  <?php endif; ?>>
					<input <?php if(in_array('B1',$reserved_seat)): ?> disabled  <?php endif; ?> type="checkbox" id="B1" value="B1"  name="seat[]">
					<label for="B1">B1</label>
				</div>
			</div>
			<div class="col-md-8 double">
				<div class="row">
					<div class="col-md-6">
				<div class="seat" <?php if(in_array('B2',$reserved_seat)): ?> style="background-color: red"  <?php endif; ?>>
					<input <?php if(in_array('B2',$reserved_seat)): ?> disabled  <?php endif; ?> type="checkbox" id="B2" value="B2"  name="seat[]">
					<label for="B2">B2</label>
						</div>
					</div>
					<div class="col-md-6">
				<div class="seat" <?php if(in_array('B3',$reserved_seat)): ?> style="background-color: red"  <?php endif; ?>>
					<input <?php if(in_array('B3',$reserved_seat)): ?> disabled  <?php endif; ?> type="checkbox" id="B3" value="B3"  name="seat[]">
					<label for="B3">B3</label>
						</div>

					</div>
				</div>
			</div>
		</div>

		<!-- C Row -->
		
		<div class="row">
			<div class="col-md-4 single">
				<div class="seat" <?php if(in_array('C1',$reserved_seat)): ?> style="background-color: red"  <?php endif; ?>>
					<input <?php if(in_array('C1',$reserved_seat)): ?> disabled  <?php endif; ?> type="checkbox" id="C1" value="C1"  name="seat[]">
					<label for="C1">C1</label>
				</div>
			</div>
			<div class="col-md-8 double">
				<div class="row">
					<div class="col-md-6">
				<div class="seat" <?php if(in_array('C2',$reserved_seat)): ?> style="background-color: red"  <?php endif; ?>>
					<input <?php if(in_array('C2',$reserved_seat)): ?> disabled  <?php endif; ?> type="checkbox" id="C2" value="C2"  name="seat[]">
					<label for="C2">C2</label>
						</div>
					</div>
					<div class="col-md-6">
				<div class="seat" <?php if(in_array('C3',$reserved_seat)): ?> style="background-color: red"  <?php endif; ?>>
					<input <?php if(in_array('C3',$reserved_seat)): ?> disabled  <?php endif; ?> type="checkbox" id="C3" value="C3"  name="seat[]">
					<label for="C3">C3</label>
						</div>
						</div>
				</div>
			</div>
		</div>

						<!-- D Row -->
		
		<div class="row">
			<div class="col-md-4 single">				
				<div class="seat" <?php if(in_array('D1',$reserved_seat)): ?> style="background-color: red"  <?php endif; ?>>
					<input <?php if(in_array('D1',$reserved_seat)): ?> disabled  <?php endif; ?> type="checkbox" id="D1" value="D1"  name="seat[]">
					<label for="D1">D1</label>
				</div>

			</div>
			<div class="col-md-8 double">
				<div class="row">
					<div class="col-md-6">				
				<div class="seat" <?php if(in_array('D2',$reserved_seat)): ?> style="background-color: red"  <?php endif; ?>>
					<input <?php if(in_array('D2',$reserved_seat)): ?> disabled  <?php endif; ?> type="checkbox" id="D2" value="D2"  name="seat[]">
					<label for="D2">D2</label>
						</div>
					</div>
					<div class="col-md-6">				
				<div class="seat" <?php if(in_array('D3',$reserved_seat)): ?> style="background-color: red"  <?php endif; ?>>
					<input <?php if(in_array('D3',$reserved_seat)): ?> disabled  <?php endif; ?> type="checkbox" id="D3" value="D3"  name="seat[]">
					<label for="D3">D3</label>
						</div>
						</div>
				</div>
			</div>
		</div>

						<!-- E Row -->
		
		<div class="row">
			<div class="col-md-4 single">				
				<div class="seat" <?php if(in_array('E1',$reserved_seat)): ?> style="background-color: red"  <?php endif; ?>>
					<input <?php if(in_array('E1',$reserved_seat)): ?> disabled  <?php endif; ?> type="checkbox" id="E1" value="E1" name="seat[]">
					<label for="E1">E1</label>
				</div>
			</div>
			<div class="col-md-8 double">
				<div class="row">
					<div class="col-md-6">				
				<div class="seat" <?php if(in_array('E2',$reserved_seat)): ?> style="background-color: red"  <?php endif; ?>>
					<input <?php if(in_array('E2',$reserved_seat)): ?> disabled  <?php endif; ?> type="checkbox" id="E2" value="E2"  name="seat[]">
					<label for="E2">E2</label>
						</div>
					</div>
					<div class="col-md-6">				
				<div class="seat" <?php if(in_array('E3',$reserved_seat)): ?> style="background-color: red"  <?php endif; ?>>
					<input <?php if(in_array('E3',$reserved_seat)): ?> disabled  <?php endif; ?> type="checkbox" id="E3" value="E3"  name="seat[]">
					<label for="E3">E3</label>
						</div>
						</div>
				</div>
			</div>
		</div>

						<!-- F Row -->
		
		<div class="row">
			<div class="col-md-4 single">				
				<div class="seat" <?php if(in_array('F1',$reserved_seat)): ?> style="background-color: red"  <?php endif; ?>>
					<input <?php if(in_array('F1',$reserved_seat)): ?> disabled  <?php endif; ?> type="checkbox" id="F1" value="F1"  name="seat[]">
					<label for="F1">F1</label>
				</div>
			</div>
			<div class="col-md-8 double">
				<div class="row">
					<div class="col-md-6">				
				<div class="seat" <?php if(in_array('F2',$reserved_seat)): ?> style="background-color: red"  <?php endif; ?>>
					<input <?php if(in_array('F2',$reserved_seat)): ?> disabled  <?php endif; ?> type="checkbox" id="F2" value="F2"  name="seat[]">
					<label for="F2">F2</label>
						</div>
					</div>
					<div class="col-md-6">				
				<div class="seat" <?php if(in_array('F3',$reserved_seat)): ?> style="background-color: red"  <?php endif; ?>>
					<input <?php if(in_array('F3',$reserved_seat)): ?> disabled  <?php endif; ?> type="checkbox" id="F3" value="F3"  name="seat[]">
					<label for="F3">F3</label>
						</div>
						</div>
				</div>
			</div>
		</div>

						<!-- G Row -->
		
		<div class="row">
			<div class="col-md-4 single">				
				<div class="seat" <?php if(in_array('G1',$reserved_seat)): ?> style="background-color: red"  <?php endif; ?>>
					<input <?php if(in_array('G1',$reserved_seat)): ?> disabled  <?php endif; ?> type="checkbox" id="G1" value="G1"  name="seat[]">
					<label for="G1">G1</label>
				</div>
			</div>
			<div class="col-md-8 double">
				<div class="row">
					<div class="col-md-6">				
				<div class="seat" <?php if(in_array('G2',$reserved_seat)): ?> style="background-color: red"  <?php endif; ?>>
					<input <?php if(in_array('G2',$reserved_seat)): ?> disabled  <?php endif; ?> type="checkbox" id="G2" value="G2"  name="seat[]"  name="seat[]"> 
					<label for="G2">G2</label>
						</div>
					</div>
					<div class="col-md-6">				
				<div class="seat" <?php if(in_array('G3',$reserved_seat)): ?> style="background-color: red"  <?php endif; ?>>
					<input <?php if(in_array('G3',$reserved_seat)): ?> disabled  <?php endif; ?> type="checkbox" id="G3" value="G3"  name="seat[]"  name="seat[]">
					<label for="G3">G3</label>
						</div>
						</div>
				</div>
			</div>
		</div>

						<!-- H Row -->
		
		<div class="row">
			<div class="col-md-4 single">				
				<div class="seat" <?php if(in_array('H1',$reserved_seat)): ?> style="background-color: red"  <?php endif; ?>>
					<input <?php if(in_array('H1',$reserved_seat)): ?> disabled  <?php endif; ?> type="checkbox" id="H1" value="H1"  name="seat[]"  name="seat[]">
					<label for="H1">H1</label>
				</div>
			</div>
			<div class="col-md-8 double">
				<div class="row">
					<div class="col-md-6">				
				<div class="seat" <?php if(in_array('H2',$reserved_seat)): ?> style="background-color: red"  <?php endif; ?>>
					<input <?php if(in_array('H2',$reserved_seat)): ?> disabled  <?php endif; ?> type="checkbox" id="H2" value="H2"  name="seat[]">
					<label for="H2">H2</label>
						</div>
					</div>
					<div class="col-md-6">				
				<div class="seat" <?php if(in_array('H3',$reserved_seat)): ?> style="background-color: red"  <?php endif; ?>>
					<input <?php if(in_array('H3',$reserved_seat)): ?> disabled  <?php endif; ?> type="checkbox" id="H3" value="H3"  name="seat[]">
					<label for="H3">H3</label>
						</div>
						</div>
				</div>
			</div>
		</div>

						<!-- I Row -->
		
		<div class="row">
			<div class="col-md-4 single">				
				<div class="seat" <?php if(in_array('I1',$reserved_seat)): ?> style="background-color: red"  <?php endif; ?>>
					<input <?php if(in_array('I1',$reserved_seat)): ?> disabled  <?php endif; ?> type="checkbox" id="I1" value="I1"  name="seat[]">
					<label for="I1">I1</label>
				</div>
			</div>
			<div class="col-md-8 double">
				<div class="row">
					<div class="col-md-6">				
				<div class="seat" <?php if(in_array('I2',$reserved_seat)): ?> style="background-color: red"  <?php endif; ?>>
					<input <?php if(in_array('I2',$reserved_seat)): ?> disabled  <?php endif; ?> type="checkbox" id="I2" value="I2"  name="seat[]">
					<label for="I2">I2</label>
						</div>
					</div>
					<div class="col-md-6">				
				<div class="seat" <?php if(in_array('I3',$reserved_seat)): ?> style="background-color: red"  <?php endif; ?>>
					<input <?php if(in_array('I3',$reserved_seat)): ?> disabled  <?php endif; ?> type="checkbox" id="I3" value="I3"  name="seat[]">
					<label for="I3">I3</label>
						</div>
						</div>
				</div>
			</div>
		</div>

						<!-- j Row -->
		
		<div class="row">
			<div class="col-md-4 single">				
				<div class="seat" <?php if(in_array('J1',$reserved_seat)): ?> style="background-color: red"  <?php endif; ?>>
					<input <?php if(in_array('J1',$reserved_seat)): ?> disabled  <?php endif; ?> type="checkbox" id="J1" value="J1"  name="seat[]">
					<label for="J1">J1</label>
				</div>
			</div>
			<div class="col-md-8 double">
				<div class="row">
					<div class="col-md-6">				
				<div class="seat" <?php if(in_array('J2',$reserved_seat)): ?> style="background-color: red"  <?php endif; ?>>
					<input <?php if(in_array('J2',$reserved_seat)): ?> disabled  <?php endif; ?> type="checkbox" id="J2" value="J2"  name="seat[]">
					<label for="J2">J2</label>
						</div>
					</div>
					<div class="col-md-6">				
				<div class="seat" <?php if(in_array('J3',$reserved_seat)): ?> style="background-color: red"  <?php endif; ?>>
					<input <?php if(in_array('J3',$reserved_seat)): ?> disabled  <?php endif; ?> type="checkbox" id="J3" value="J3"  name="seat[]">
					<label for="J3">J3</label>
						</div>






					</div>
				</div>
			</div>
		</div>
		<button type="submit" class="btn btn-primary">Continue</button>

	</div>
</div>


 



</form>




<?php $__env->stopSection(); ?>

<?php echo $__env->make('frontend.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\booking-system\resources\views/frontend/layouts/showseat.blade.php ENDPATH**/ ?>