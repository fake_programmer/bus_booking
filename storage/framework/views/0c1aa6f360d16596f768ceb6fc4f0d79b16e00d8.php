<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
    <meta name="generator" content="Jekyll v3.8.5">
    <title>Dashboard</title>


    <!-- Custom styles for this template -->
    <link href="<?php echo e(asset('/css/dashboard.css')); ?>" rel="stylesheet">
    <link href="<?php echo e(asset('/css/bootstrap.min.css')); ?>" rel="stylesheet">

      <link rel="shortcut icon" href="<?php echo e(url ('assets/img/favicon.ico')); ?>" type="image/x-icon" />

      <!-- Google fonts include -->
      <link href="<?php echo e(url('https://fonts.googleapis.com/css?family=Rubik:300,300i,400,400i,500,500i,700,900')); ?>" rel="stylesheet">
      <link href="<?php echo e(url('/css/vendor.css')); ?>" rel="stylesheet">


      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

      <link href="<?php echo e(asset('css/bootstrap.min.css')); ?>" rel="stylesheet" media="screen">
      <link href="<?php echo e(asset('css/font-awesome.css')); ?>" rel="stylesheet" media="screen">
      <link href="<?php echo e(asset('css/bootstrap-datetimepicker.min.css')); ?>" rel="stylesheet" media="screen">
      <link rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.14/themes/base/jquery-ui.css" type="text/css" media="all">

      <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/css/bootstrap-datetimepicker.css" rel="stylesheet" media="screen">

      <!-- Main Style CSS -->
      
  </head>
  <body>
   <?php echo $__env->make('backend.partials.nav', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

<div class="container-fluid">
  <div class="row">
    <?php echo $__env->make('backend.partials.sidebar', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

    <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">

      <?php echo $__env->yieldContent('content'); ?>

    </main>
  </div>
</div>
   <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/js/bootstrap-datetimepicker.min.js"></script>
   <script src="<?php echo e(asset('/js/jquery-3.4.1.min.js')); ?>" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
   <script>window.jQuery || document.write('<script src="/docs/4.3/assets/js/vendor/jquery-slim.min.js"><\/script>')</script><script src="/docs/4.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-xrRywqdh3PHs8keKZN+8zzc5TX0GRTLCcmivcbNJWm2rs5C8PRhcEn3czEjhAO9o" crossorigin="anonymous"></script>
   <script src="<?php echo e(asset('js/jquery.js')); ?>"></script>

   <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js  "></script>
   <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"></script>
   <script type="text/javascript" src="<?php echo e(asset('/js/moment.js')); ?>"></script>
   <script type="text/javascript" src="<?php echo e(asset('/js/jquery-3.4.1.min.js')); ?>"></script>

   <script src="<?php echo e(asset('vendors/js/vendor.bundle.base.js')); ?>"></script>
   <script src="<?php echo e(asset('vendors/js/vendor.bundle.addons.js')); ?>"></script>
   <script src="<?php echo e(asset('js/off-canvas.js')); ?>"></script>
   <script src="<?php echo e(asset('js/misc.js')); ?>"></script>
   <script src="<?php echo e(asset('js/jquery.js')); ?>"></script>
   <script type="text/javascript" src="<?php echo e(asset('/js/bootstrap.min.js')); ?>"></script>
   <script src="<?php echo e(asset('/js/vendor.js')); ?>"></script>
   <script src="<?php echo e(asset('/js/active.js')); ?>"></script>

   <script type="text/javascript" src="<?php echo e(asset('/js/bootstrap.min.js')); ?>"></script>
   <script src="<?php echo e(asset('js/bootstrap.bundle.min.js')); ?>"></script>
   <script src="<?php echo e(asset('/js/feather.min.js')); ?>"></script>
   <script src="<?php echo e(asset('/js/chart.min.js')); ?>"></script>
   <script src="<?php echo e(asset('/js/dashboard.js')); ?>"></script>
  <?php echo $__env->yieldContent('script'); ?>
      </body>
</html>
<?php /**PATH F:\bookingsystem\resources\views/backend/master.blade.php ENDPATH**/ ?>