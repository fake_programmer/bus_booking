<?php $__env->startSection('content'); ?>

  <style>
  h1{
    text-align: center;
  };
  
</style>

<!-- Button trigger modal -->

<h1>Edit Locations</h1> 
<br><br>

<div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Edit Location</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="<?php echo e(route('editlocationprocess', $locations->id)); ?>" method="POST">
      <?php echo csrf_field(); ?>
           <div class="modal-body">
        <div class="modal-body">
          <div class="form-group">
            <label for="formGroupExampleInput">Location Name</label>
            <input type="text" class="form-control" name="name" id="formGroupExampleInput" placeholder="Location Name" value="<?php echo e($locations->name); ?>">
          </div>
          <div class="form-group">
            <label for="formGroupExampleInput">Status</label>
            <select name="status" class="form-control" id="formGroupExampleInput">
              <option value="Active">Active</option>
              <option value="Inactive">Inactive</option>
            </select>
          </div>
          <div> 
          </div>
        </div>

        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary">Submit</button>
        </div>
      </form>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('script'); ?>
  <script>
    $(document).ready(function(){
      $('#orderTable').DataTable();
    });

  </script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('backend.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH F:\bookingsystem\resources\views/backend/editlocation.blade.php ENDPATH**/ ?>